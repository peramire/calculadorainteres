package root.model;

public class CalcularInteres {
    
    public String getInteres(String capital, String tasa, String anos)
    {
        double calc = Double.parseDouble(tasa) / 100;
        calc = calc * Double.parseDouble(capital) * Double.parseDouble(anos);
        String res = String.valueOf(calc);
        return res;
    }
    
}
