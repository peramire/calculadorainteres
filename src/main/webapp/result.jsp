<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RESULTADO CALCULO</title>
    </head>
    <body>
        <h1>Resultado Calculo Intereses</h1>
        <br>
        <%
           String capital = (String) request.getAttribute("capital");
           String anos = (String) request.getAttribute("anos");
           String tasa = (String) request.getAttribute("tasa");
           String resultado = (String) request.getAttribute("resultado");
        %>
        <table>
            <tr>
                <td>Capital : </td>
                <td>$ <%=capital%></td>
            </tr>
            <tr>
                <td>Numero de Años : </td>
                <td><%=anos%></td>
            </tr>
            <tr>
                <td>Tasa Interes Anual : </td>
                <td><%=tasa%> % </td>
            </tr>
            <tr>
                <td><b>Cada Cuota Tendrá un Interés de : </b></td>
                <td><b><%=resultado%></b></td>
            </tr>
        </table>
    </body>
</html>
