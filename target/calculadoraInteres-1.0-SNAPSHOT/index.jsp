<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculo Interes</title>
    </head>
    <script>
        function esEntero(){            
            if (isNaN(parseInt(window.document.form.capital.value))) {
            alert ("Debe ingresar número entero");
            window.document.form.capital.value = "";
            window.document.form.capital.focus();    
            return false;
            }
            
            if (isNaN(parseInt(window.document.form.tasa.value))) {
            alert ("Debe ingresar número entero");
            window.document.form.tasa.value = "";
            window.document.form.tasa.focus();    
            return false;
            }
        }
    </script>
    <body>
        <h1>Calculadora de Intereses</h1>
        <form name="form" action="controller" method="POST">
        <table>
            <tr>
                <td>Capital : </td>
                <td>$ <input type="text" name="capital" maxlength="7" value="" /></td>
            </tr>
            <tr>
                <td>Numero de Años : </td>
                <td>
                    <select name="anos">
                      <option value="1">1 año</option>
                      <option value="2">2 años</option>
                      <option value="3">3 años</option>
                      <option value="4">4 años</option>
                      <option value="5">5 años</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tasa Interes Anual : </td>
                <td><input type="text" name="tasa" maxlength="2" value="" /> % </td>
            </tr>
            <tr>
                <td><input type="submit" value="Calcular" onclick="return esEntero();"></td>
            </tr>
            </form>
        </table>
    </body>
</html>
